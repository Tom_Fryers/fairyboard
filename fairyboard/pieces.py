from .chess import PieceType

PIECES = [
    PieceType("king", None),
    PieceType("queen", "Q"),
    PieceType("rook", "R"),
    PieceType("bishop", "B"),
    PieceType("knight", "N"),
    PieceType("amazon", "QN"),
    PieceType("empress", "RN", "chancellor"),
    PieceType("princess", "BN", "archbishop"),
    PieceType("leaping bishop", "BD", None),
    PieceType("mann", "K", "commoner"),
    PieceType("wazir", "W"),
    PieceType("ferz", "F", "fers"),
    PieceType("pawn", None),
    PieceType("dabbaba", "D", None),
    PieceType("alfil", "A"),
    PieceType("nightrider", "NN", None),
    PieceType("queen of the night", "QNN", None),
    PieceType("woody", "WD", None),
    PieceType("crowned empress", "RNF", "aiwok"),
    PieceType("centaur", "NK"),
    PieceType("crowned rook", "RF", "bers"),
    PieceType("crowned bishop", "BW", "dragonHorse"),
    PieceType("shogi pawn", "fW"),
    PieceType("lance", "fR", "lance"),
    PieceType("kniroo", "mNcR"),
    PieceType("knibis", "mNcB"),
    PieceType("silver", "FfW", "silver"),
    PieceType("gold", "WfF", "gold"),
    PieceType("rookni", "mRcN"),
    PieceType("biskni", "mBcN"),
]


def make_piece(piece_type, colour):
    return next(p for p in PIECES if p.name == piece_type).create(colour)
