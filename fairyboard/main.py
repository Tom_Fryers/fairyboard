"""Fairyboard is a GUI frontend for Fairy Stockfish."""
import argparse
import itertools
import re

import pygame
from cvectors import Vector

from . import chess, engine, ui
from .chess import B, W
from .pieces import PIECES
from .variants import VARIANTS


class Config:
    def __init__(self):
        parser = argparse.ArgumentParser(description=__doc__)
        parser.add_argument(
            "-w",
            "--white",
            choices=("human", "engine"),
            default="engine",
            help="white player",
        )
        parser.add_argument(
            "-b",
            "--black",
            choices=("human", "engine"),
            default="engine",
            help="black player",
        )
        parser.add_argument(
            "-wt",
            "--white-time",
            type=int,
            default=60,
            help="white clock time (seconds)",
        )
        parser.add_argument(
            "-bt",
            "--black-time",
            type=int,
            default=60,
            help="black clock time (seconds)",
        )
        parser.add_argument(
            "-wi",
            "--white-increment",
            type=int,
            default=1,
            help="white increment (seconds)",
        )
        parser.add_argument(
            "-bi",
            "--black-increment",
            type=int,
            default=1,
            help="black increment (seconds)",
        )
        parser.add_argument(
            "--base-variant",
            choices=VARIANTS,
            default="chess",
            help="variant to base game on",
        )
        parser.add_argument("--extra-pieces", help="extra pieces (comma-separated)")
        args = parser.parse_args()
        self.players = (args.white, args.black)
        self.times = (args.white_time, args.black_time)
        self.increments = (args.white_increment, args.black_increment)
        self.base_variant = VARIANTS[args.base_variant]
        if args.extra_pieces is not None:
            self.pieces = args.extra_pieces.split(",")
        else:
            self.pieces = []


config = Config()
S = pygame.display.set_mode((1280, 720), pygame.RESIZABLE)
pygame.display.set_caption("Fairyboard")

PIECES += config.base_variant.extra_pieces + [
    chess.PieceType(p, p, p) for p in config.pieces
]
PIECE_NAMES = {a.name: a for a in PIECES}

ui.set_icon(PIECES[5])


class GameBoard(ui.GUIBoard):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.move = W
        variant = config.base_variant()
        self.width = variant.board.width
        self.height = variant.board.height
        self.squares = variant.board.squares
        self.castling = ["K", "Q", "k", "q"]
        self.half_moves = 0
        self.full_moves = 1
        self.en_passant = "-"

    def set_castling(self):
        self.castling = []
        for colour in "wb":
            row = []
            for file_ in range(self.width):
                piece = self[self.square(file_, 0 if colour == W else self.height - 1)]
                try:
                    if piece.colour == colour:
                        row.append({"rook": "r", "king": "k"}[piece.name])
                except (KeyError, AttributeError):
                    continue
            row = "".join(row)
            if re.search(r"r.*k", row):
                self.castling.append("Qq"[colour])
            if re.search(r"k.*r", row):
                self.castling.append("Kk"[colour])

    def set_size(self, width, height):
        new = [None for _ in range(width * height)]
        for file_, rank in itertools.product(
            range(min(width, self.width)), range(min(height, self.height))
        ):
            new[chess.Square(file_, rank, width, height).index] = self[
                self.square(file_, rank)
            ]
        self.squares = new
        self.width = width
        self.height = height
        self.set_castling()

    def make_move(self, from_square, to_square, promotion=None):
        if (
            from_square is not None
            and self[from_square].name == "king"
            and (to_square - from_square)
            in {
                (2, 0),
                (-2, 0),
            }
        ):
            side = (to_square.file_num - from_square.file_num) // 2
            killed_king = None
            killed_rook = None
            for square in range(
                from_square.file_num + 1
                if side == -1
                else self.width - from_square.file_num
            ):
                square = from_square + (square * side, 0)
                if self[square] is not None and self[square].colour == self.move:
                    if self[square].name == "rook" and killed_rook is None:
                        killed_rook = self[square]
                        self[square] = None
                    elif self[square].name == "king" and killed_king is None:
                        killed_king = self[square]
                        self[square] = None
            self[to_square] = killed_king
            self[from_square + (side, 0)] = killed_rook
            self.castling = [x for x in self.castling if x.islower() == self.move]
        else:
            if from_square is not None:
                if self[from_square].name == "king":
                    self.castling = [
                        x for x in self.castling if x.islower() == self.move
                    ]
                elif self[from_square].name == "rook":
                    try:
                        king_pos = next(
                            x
                            for x in range(self.width)
                            if (square := self[self.square(x, from_square.rank_num)])
                            is not None
                            and square.name == "king"
                        )
                        side = "k" if from_square.file_num > king_pos else "q"
                        side = side.upper() if self.move == W else side
                        if side in self.castling:
                            self.castling.remove(side)
                    except StopIteration:
                        pass
            self[to_square] = promotion if promotion is not None else self[from_square]
            if from_square is not None:
                self[from_square] = None
        self.move = 1 - self.move

    @property
    def fen_castling(self):
        return "".join(self.castling) if self.castling else "-"

    def fen(self, piece_letters):
        fen = []
        for rank in range(self.height - 1, -1, -1):
            rank_text = []
            blanks = 0
            for file_ in range(self.width):
                piece = self[self.square(file_, rank)]
                if piece is None:
                    blanks += 1
                    continue
                if blanks:
                    rank_text.append(str(blanks))
                blanks = 0
                rank_text.append(
                    piece_letters[piece.piece_type].lower()
                    if piece.colour == B
                    else piece_letters[piece.piece_type]
                )
            if blanks:
                rank_text.append(str(blanks))
            fen.append("".join(rank_text))
        return " ".join(
            (
                "/".join(fen),
                "wb"[self.move],
                self.fen_castling,
                self.en_passant,
                str(self.half_moves),
                str(self.full_moves),
            )
        )


class AppExit(BaseException):
    pass


class App:
    def __init__(self):
        self.analysis_font = pygame.font.Font(ui.FONT, 35)
        self.board = GameBoard(8, 8, pieces=PIECES)
        self.set_piece_letters(
            {self.board[s].piece_type for s in self.board if self.board[s]}
        )
        self.engine = engine.Engine()
        self.engine.load_variant(
            self.board.fen(self.piece_letters),
            self.board.width,
            self.board.height,
            PIECES,
            self.piece_letters,
            config.base_variant,
        )
        self.engine.analyse()
        self.piece_shop = ui.PieceShop(4, self.board.height, pieces=PIECES)

        self.mode_switch = ui.TextSwitch(
            options=("Mode: Edit", "Mode: Move", "Mode: Play"),
            position=(0, 0),
            font=self.analysis_font,
            colours=ui.BUTTON_COLOURS,
        )
        self.move_switch = ui.TextSwitch(
            options=("Move: \N{white circle}", "Move: \N{black circle}"),
            position=(0, 0),
            font=self.analysis_font,
            colours=ui.BUTTON_COLOURS,
        )
        self.buttons = (self.mode_switch, self.move_switch)

        self.players = None

        self.cursor_piece = None
        self.move = None
        self.highlight = None

        self.clock = pygame.time.Clock()
        self.clocks = None
        self.recalculate_square_size()

    def set_piece_letters(self, pieces):
        self.piece_letters = {}
        for piece in PIECES:
            if piece in pieces:
                self.piece_letters[piece] = next(
                    a
                    for a in piece.best_letters
                    if a not in self.piece_letters.values()
                )

    def recalculate_square_size(self):
        global square_size
        square_size = min(
            S.get_height() // self.board.height,
            (S.get_width() - ui.TEXT_WIDTH)
            // (self.board.width + self.piece_shop.width),
        )
        self.piece_shop.square_size = square_size
        self.board.square_size = square_size
        self.piece_shop_pos = Vector(
            S.get_width() - self.piece_shop.width * square_size, 0
        )
        self.board_width = square_size * self.board.width
        self.mode_switch.position = (self.board_width + 2, 400)
        self.move_switch.position = (self.board_width + 2, 445)

    def run(self):
        try:
            while True:
                self.process_events()
                self.draw()
        except AppExit:
            return

    def switch_move(self):
        self.board.move = 1 - self.board.move
        self.move_switch.index = self.board.move
        self.engine.set_board(self.board.fen(self.piece_letters))

    def handle_universal_event(self, event):
        if event.type == pygame.QUIT:
            raise AppExit
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_q:
                raise AppExit
        elif event.type == pygame.VIDEORESIZE:
            self.recalculate_square_size()
        elif event.type == pygame.MOUSEBUTTONDOWN:
            for button in self.buttons:
                button.click(self.mouse_pos)
        elif event.type == pygame.MOUSEBUTTONUP:
            if self.move_switch.release(self.mouse_pos):
                self.switch_move()
            if self.mode_switch.release(self.mouse_pos):
                if "Edit" in self.mode_switch.text:
                    self.engine.analyse()
                    self.players = None
                    self.move_switch.enabled = True
                elif "Move" in self.mode_switch.text:
                    self.move_switch.enabled = False
                    self.cursor_piece = None
                else:
                    self.play()

    def play(self):
        self.players = config.players
        self.clocks = [
            chess.Clock(config.times[s], config.increments[s]) for s in range(2)
        ]
        self.game_result = None
        if self.players[B] == self.players[W] == "engine":
            self.engine.play(
                self.board.move,
                move=self.board.move,
                clock=self.clocks[self.board.move],
            )
        elif self.players[W] == "engine":
            self.engine.play(W, move=self.board.move, clock=self.clocks[W])
        elif self.players[B] == "engine":
            self.engine.play(B, move=self.board.move, clock=self.clocks[B])
        else:
            self.engine.send_messages(["force"])
        self.clocks[self.board.move].start()

    def handle_edit_mode_event(self, event):
        if event.type == pygame.MOUSEBUTTONUP:
            pre_pieces = set(self.piece_letters)
            pre_fen = self.board.fen(self.piece_letters)
            shop_square = self.piece_shop.square_from_mouse_pos(
                self.mouse_pos - self.piece_shop_pos
            )
            if shop_square is not ui.OFF_BOARD:
                if event.button != pygame.BUTTON_RIGHT:
                    self.cursor_piece = (
                        self.piece_shop[shop_square].copy()
                        if self.piece_shop[shop_square] is not None
                        else None
                    )
                return
            board_square = self.board.square_from_mouse_pos(self.mouse_pos)
            if board_square is ui.OFF_BOARD:
                if event.button != pygame.BUTTON_RIGHT:
                    self.cursor_piece = None
                return
            if event.button == pygame.BUTTON_RIGHT:
                self.cursor_piece = self.board[board_square]
                return
            if event.button == pygame.BUTTON_LEFT:
                self.board[board_square], self.cursor_piece = (
                    self.cursor_piece,
                    self.board[board_square],
                )
            elif event.button == pygame.BUTTON_MIDDLE:
                self.board[board_square] = self.cursor_piece

            new_pieces = {self.board[s].piece_type for s in self.board if self.board[s]}

            if new_pieces != pre_pieces:
                self.set_piece_letters(new_pieces)
                self.engine.load_variant(
                    self.board.fen(self.piece_letters),
                    self.board.width,
                    self.board.height,
                    PIECES,
                    self.piece_letters,
                    config.base_variant,
                )
                self.engine.analyse()
            elif pre_fen != self.board.fen(self.piece_letters):
                self.board.set_castling()
                self.engine.set_board(self.board.fen(self.piece_letters))

        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_TAB:
                self.piece_shop.swap_colours()
            elif event.key == pygame.K_m:
                self.switch_move()
            elif event.key in ui.SIZE_CHANGES:
                change = ui.SIZE_CHANGES[event.key]
                self.board.set_size(
                    self.board.width + change[0], self.board.height + change[1]
                )
                self.engine.load_variant(
                    self.board.fen(self.piece_letters),
                    self.board.width,
                    self.board.height,
                    PIECES,
                    self.piece_letters,
                    config.base_variant,
                )
                self.engine.analyse()
                if self.board.height != self.piece_shop.height:
                    self.piece_shop = ui.PieceShop(4, self.board.height, pieces=PIECES)
                self.recalculate_square_size()

    def handle_move_mode_event(self, event):
        if event.type == pygame.MOUSEBUTTONUP:
            if self.cursor_piece is None:
                self.cursor_piece = self.board.piece_clicked(
                    self.mouse_pos, piece=ui.CLEAR_SQUARE
                )
                if self.cursor_piece is None or self.cursor_piece is ui.OFF_BOARD:
                    self.cursor_piece = None
                    return
                self.old_square = self.board.square_from_mouse_pos(self.mouse_pos)
                self.highlight = ui.Highlight(self.engine.lift(str(self.old_square)))
                if not self.highlight:
                    self.board[self.old_square] = self.cursor_piece
                    self.cursor_piece = None
                    self.highlight = None
                return
            self.new_square = self.board.square_from_mouse_pos(self.mouse_pos)
            self.board[self.old_square] = self.cursor_piece
            if all(
                (
                    self.old_square != self.new_square,
                    self.new_square is not ui.OFF_BOARD,
                    self.new_square in self.highlight,
                )
            ):
                self.board.piece_clicked(self.mouse_pos, piece=self.cursor_piece)
                self.board.make_move(self.old_square, self.new_square)
                self.move_switch.index = 1 - self.move_switch.index
                self.engine.send_move(str(self.old_square) + str(self.new_square))
            self.highlight = None
            self.cursor_piece = None

    def handle_play_mode_event(self, event):
        if self.players[self.board.move] != "human":
            return
        if event.type == pygame.MOUSEBUTTONUP:
            if self.cursor_piece is None:
                self.cursor_piece = self.board.piece_clicked(
                    self.mouse_pos, piece=ui.CLEAR_SQUARE
                )
                if self.cursor_piece is None or self.cursor_piece is ui.OFF_BOARD:
                    self.cursor_piece = None
                    return
                self.old_square = self.board.square_from_mouse_pos(self.mouse_pos)
                self.highlight = ui.Highlight(self.engine.lift(str(self.old_square)))
                if not self.highlight:
                    self.board[self.old_square] = self.cursor_piece
                    self.cursor_piece = None
                    self.highlight = None
                return
            self.new_square = self.board.square_from_mouse_pos(self.mouse_pos)
            self.board[self.old_square] = self.cursor_piece
            if all(
                (
                    self.old_square != self.new_square,
                    self.new_square is not ui.OFF_BOARD,
                    self.new_square in self.highlight,
                )
            ):
                self.clocks[self.board.move].end()
                self.board.piece_clicked(self.mouse_pos, piece=self.cursor_piece)
                self.board.make_move(self.old_square, self.new_square)
                self.move_switch.index = 1 - self.move_switch.index
                self.engine.set_time(self.clocks[self.board.move])
                self.engine.send_move(str(self.old_square) + str(self.new_square))
                self.clocks[self.board.move].start()
            self.highlight = None
            self.cursor_piece = None

    def handle_force_engine_move(self, event):
        if (
            event.type == pygame.KEYDOWN
            and event.key == pygame.K_RETURN
            and self.move is not None
        ):
            self.board.make_move(*self.move)
            self.move_switch.index = 1 - self.move_switch.index
            self.engine.send_move(self.move_string)

    def process_events(self):
        self.mouse_pos = Vector(*pygame.mouse.get_pos())
        self.cursor = pygame.SYSTEM_CURSOR_ARROW
        for button in self.buttons:
            if button.mouse_over(self.mouse_pos):
                self.cursor = pygame.SYSTEM_CURSOR_HAND
        for event in pygame.event.get():
            self.handle_universal_event(event)
            if "Edit" in self.mode_switch.text:
                self.handle_edit_mode_event(event)
                self.handle_force_engine_move(event)
            elif "Move" in self.mode_switch.text:
                self.handle_move_mode_event(event)
                self.handle_force_engine_move(event)
            elif "Play" in self.mode_switch.text:
                self.handle_play_mode_event(event)
        if "Edit" in self.mode_switch.text:
            piece_shop_square = self.piece_shop.square_from_mouse_pos(
                self.mouse_pos - self.piece_shop_pos
            )
            piece_over = (
                self.piece_shop[piece_shop_square]
                if piece_shop_square is not ui.OFF_BOARD
                else None
            )
        else:
            piece_over = None
        if piece_over is None:
            board_square = self.board.square_from_mouse_pos(self.mouse_pos)
            piece_over = (
                self.board[board_square] if board_square is not ui.OFF_BOARD else None
            )
        if self.cursor_piece or (
            piece_over is not None
            and piece_over is not ui.OFF_BOARD
            and self.is_piece_movable(piece_over)
        ):
            self.cursor = pygame.SYSTEM_CURSOR_HAND

    def is_piece_movable(self, piece):
        return "Edit" in self.mode_switch.text or (
            self.board.move == piece.colour
            and (
                "Move" in self.mode_switch.text
                or self.players[self.board.move] == "human"
            )
        )

    def decode_piece(self, letter):
        return next(
            p for p in self.piece_letters if self.piece_letters[p] == letter.upper()
        ).create(self.board.move)

    def decode_square(self, square):
        return chess.Square.from_text(square, self.board.width, self.board.height)

    def decode_move(self, string):
        if "@" in string:
            return [None, self.decode_square(string[2:4]), self.decode_piece(string[0])]
        move = [self.decode_square(m) for m in (string[:2], string[2:4])]
        if len(string) == 5:
            move.append(self.decode_piece(string[4]))
        return move

    def draw_text_lines(self, text):
        ui.render_lines(
            S.subsurface(
                (
                    self.board_width + 2,
                    2,
                    S.get_width() - self.board_width - 2,
                    S.get_height() - 2,
                )
            ),
            text,
            self.analysis_font,
            45,
        )

    def draw(self):
        pygame.mouse.set_system_cursor(self.cursor)
        S.fill((255, 255, 255))
        board_image = S.subsurface(
            (0, 0, self.board_width, square_size * self.board.height)
        )
        self.board.draw(board_image, self.highlight)
        if "Edit" in self.mode_switch.text:
            self.piece_shop.draw(
                S.subsurface(
                    (
                        self.piece_shop_pos[0],
                        self.piece_shop_pos[1],
                        self.piece_shop.width * square_size,
                        S.get_height(),
                    )
                )
            )
        for button in self.buttons:
            button.draw(S)
        if self.players is None:
            analysis = self.engine.get_latest()
            if self.engine.game_result:
                analysis_text = [self.engine.game_result]
            else:
                analysis_text = ui.get_analysis_text(analysis, self.board.move)
            self.draw_text_lines(analysis_text)

            if analysis is not None and len(analysis) > 7:
                self.move_string = analysis[7]
                self.move = self.decode_move(self.move_string)
                if self.move[0] is not None:
                    ui.render_move_arrow(S, self.move[0], self.move[1], square_size)
                if len(self.move) == 3:
                    piece_image = ui.piece_renderer.draw(self.move[2], square_size)
                    square = S.subsurface(self.board.rects[self.move[1]])
                    ui.blit_alpha(square, piece_image, 127)
            else:
                self.move = None
        else:
            if self.engine.game_result is None:
                lines = [f"{self.clocks[c]} " for c in (1, 0)]
                if self.board.move == B:
                    lines[0] += "\N{black circle}"
                else:
                    lines[1] += "\N{white circle}"
            else:
                lines = [self.engine.game_result]

            if self.players[self.board.move] == "engine":
                move = self.engine.get_move()
                if move is not None:
                    self.clocks[self.board.move].end()
                    self.board.make_move(*self.decode_move(move))
                    self.move_switch.index = 1 - self.move_switch.index
                    if self.players[self.board.move] == "engine":
                        self.engine.play(
                            self.board.move,
                            move=self.board.move,
                            clock=self.clocks[self.board.move],
                        )
                    self.clocks[self.board.move].start()
            self.draw_text_lines(lines)
            self.move = None
        if self.cursor_piece is not None:
            hover_piece = ui.piece_renderer.draw(self.cursor_piece, square_size)
            S.blit(hover_piece, self.mouse_pos - Vector(*hover_piece.get_size()) / 2)
        pygame.display.flip()
        self.clock.tick(120)
