from __future__ import annotations

from dataclasses import dataclass
from typing import ClassVar

from .chess import B, Board, PieceType, W
from .pieces import make_piece


@dataclass(init=False)
class Variant:
    name: ClassVar[str]
    board: Board
    extra_pieces: list[PieceType]


class Chess(Variant):
    name = "chess"
    extra_pieces = []

    def __init__(self):
        back_rank = (
            "rook",
            "knight",
            "bishop",
            "queen",
            "king",
            "bishop",
            "knight",
            "rook",
        )
        self.board = Board(8, 8)
        for file_ in range(self.board.width):
            self.board[self.board.square(file_, 0)] = make_piece(back_rank[file_], W)
            self.board[self.board.square(file_, 1)] = make_piece("pawn", W)
            self.board[self.board.square(file_, 6)] = make_piece("pawn", B)
            self.board[self.board.square(file_, 7)] = make_piece(back_rank[file_], B)


class Xiangqi(Variant):
    name = "xiangqi"
    extra_pieces = [
        PieceType("soldier", "fsW"),
        PieceType("elephant", "nA"),
        PieceType("cannon", "mRcpR"),
    ]

    def __init__(self):
        back_rank = (
            "rook",
            "knight",
            "elephant",
            "ferz",
            "king",
            "ferz",
            "elephant",
            "knight",
            "rook",
        )
        self.board = Board(9, 10)
        for file_ in range(self.board.width):
            self.board[self.board.square(file_, 0)] = make_piece(back_rank[file_], W)
            if file_ in (1, 7):
                self.board[self.board.square(file_, 1)] = make_piece("cannon", W)
                self.board[self.board.square(file_, 8)] = make_piece("cannon", B)
            if not file_ % 2:
                self.board[self.board.square(file_, 2)] = make_piece("soldier", W)
                self.board[self.board.square(file_, 7)] = make_piece("soldier", B)
            self.board[self.board.square(file_, 9)] = make_piece(back_rank[file_], B)


# TODO Handle promoted pieces
class Shogi(Variant):
    name = "shogi"
    extra_pieces = [PieceType("shogi knight", "fN")]

    def __init__(self):
        back_rank = (
            "lance",
            "shogi knight",
            "silver",
            "gold",
            "king",
            "gold",
            "silver",
            "shogi knight",
            "lance",
        )
        self.board = Board(9, 9)
        for file_ in range(self.board.width):
            self.board[self.board.square(file_, 0)] = make_piece(back_rank[file_], W)
            if file_ == 1:
                self.board[self.board.square(file_, 1)] = make_piece("bishop", W)
                self.board[self.board.square(file_, 7)] = make_piece("rook", B)
            elif file_ == 7:
                self.board[self.board.square(file_, 1)] = make_piece("rook", W)
                self.board[self.board.square(file_, 7)] = make_piece("bishop", B)
            self.board[self.board.square(file_, 2)] = make_piece("shogi pawn", W)
            self.board[self.board.square(file_, 6)] = make_piece("shogi pawn", B)
            self.board[self.board.square(file_, 8)] = make_piece(back_rank[file_], B)


VARIANTS = {variant.name: variant for variant in (Chess, Xiangqi, Shogi)}
