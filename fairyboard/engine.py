import concurrent.futures
import subprocess
import tempfile
from multiprocessing import cpu_count
from pathlib import Path

MEMORY = 2048
CPUS = cpu_count() - 1

custom_count = 0


def make_definition(fen, width, height, pieces, piece_letters, base):
    global custom_count
    custom_count += 1
    custom_piece_count = 0
    definition = [
        f"[custom{custom_count}:{base.name}]",
        f"startFen={fen}",
        f"maxFile={width}",
        f"maxRank={height}",
    ]

    no_kings = True
    try:
        king_letter = next(piece_letters[p] for p in piece_letters if p.name == "king")
    except StopIteration:
        pass
    else:
        board = fen.partition(" ")[0]
        no_kings = king_letter.lower() not in board or king_letter.upper() not in board
    if no_kings:
        definition += ["extinctionValue=loss", "extinctionPieceTypes=*"]
    definition.append(f"promotionRank={height}")
    promote_to = {p.lower() for p in piece_letters.values()} - {"k", "p"}
    definition.append(f"promotionPieceTypes={''.join(promote_to)}")
    for piece in pieces:
        if piece not in piece_letters:
            continue
        sf_letter = piece_letters[piece].lower()
        if piece.stockfish_name is not None:
            definition.append(f"{piece.stockfish_name}={sf_letter}")
        else:
            custom_piece_count += 1
            definition.append(
                f"customPiece{custom_piece_count}={sf_letter}:{piece.betza}"
            )
    return "\n".join(definition)


# Segfaults if castling is wrong
class Engine:
    def __init__(self):
        self.process = subprocess.Popen(
            ["fairy-stockfish"],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            text=True,
        )
        self.send_messages(
            ("xboard", "protover 2", f"cores {CPUS}", f"memory {MEMORY}")
        )
        self.excecutor = concurrent.futures.ThreadPoolExecutor()
        self.regenerate_future()
        self.go_on_move = False
        self.variant_file = None

    def analyse(self):
        self.send_messages(["analyze"])
        self.mode = "analyse"
        self.reached_analysis = False
        self.analyses = []

    def regenerate_future(self):
        self.future = self.excecutor.submit(self.process.stdout.readline)

    def send_messages(self, messages):
        self.process.stdin.write("\n".join(messages) + "\n")
        self.process.stdin.flush()

    def load_variant(self, start_fen, width, height, pieces, piece_letters, base):
        if self.variant_file is not None:
            Path(self.variant_file.name).unlink()
        self.variant_file = tempfile.NamedTemporaryFile("wt", delete=False)
        self.variant_file.write(
            make_definition(start_fen, width, height, pieces, piece_letters, base)
        )
        self.variant_file.close()
        self.send_messages(
            [
                f"option VariantPath={self.variant_file.name}",
                f"variant custom{custom_count}",
            ]
        )
        self.reached_analysis = False
        self.game_result = None

    def get_latest(self):
        while self.future.done():
            result = self.get_messages(3)
            if self.reached_analysis:
                if (
                    result
                    and not result.startswith("setup")
                    and "info string" not in result
                ):
                    self.analyses.append(result)
            else:
                self.reached_analysis = "info string" in result
        if self.analyses:
            return self.analyses[-1].split()

    def send_move(self, move):
        self.analyses = []
        if self.reached_analysis or self.mode != "analyse":
            self.send_messages([move])
        if self.go_on_move:
            self.go_on_move = False
            self.send_messages(["go"])
        self.reached_analysis = False

    def set_board(self, fen):
        self.analyses = []
        if self.reached_analysis:
            self.send_messages([f"setboard {fen}"])
        self.reached_analysis = False
        self.game_result = None

    def lift(self, square):
        self.send_messages([f"lift {square}"])

        while not (value := self.get_messages()).startswith("highlight"):
            if self.mode == "analyse":
                if self.reached_analysis:
                    self.analyses.append(value)
                elif "info string" in value:
                    self.reached_analysis = True

        return value

    def get_move(self):
        value = ""
        while True:
            if not self.future.done():
                return value[5:].strip() if value.startswith("move ") else None
            value = self.get_messages()

    def play(self, side, move, clock):
        self.send_messages(["exit"])
        self.set_time(clock)
        if side == move:
            self.send_messages(["go"])
            self.go_on_move = False
        else:
            self.go_on_move = True
        self.mode = "play"
        self.game_result = None

    def set_time(self, clock):
        time = int(clock.time)
        increment = clock.increment
        # Move as fast as possible if time is under one second
        # This stops it getting confused about zero time
        if time < 1:
            time = 1
            increment = 0
        mins, secs = divmod(time, 60)
        self.send_messages([f"level 0 {mins}:{secs} {increment}"])

    def get_messages(self, timeout=3):
        val = self.future.result(timeout)
        self.regenerate_future()
        for result in ("Draw", "White wins", "Black wins"):
            if "{" + result + "}" in val:
                self.game_result = result
        return val

    def __del__(self):
        self.send_messages(["quit"])
        Path(self.variant_file.name).unlink()
        self.process.terminate()
        self.future.cancel()
