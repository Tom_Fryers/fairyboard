import itertools
from dataclasses import dataclass, field

import pygame
from cvectors import Vector

from . import chess
from .chess import B, W

SIZE_CHANGES = {
    pygame.K_UP: (0, 1),
    pygame.K_LEFT: (-1, 0),
    pygame.K_DOWN: (0, -1),
    pygame.K_RIGHT: (1, 0),
}
CLEAR_SQUARE = object()
OFF_BOARD = object()

AA = 2

TEXT_WIDTH = 200
BUTTON_COLOURS = {"default": (0, 0, 0), "active": (130, 0, 200), "hover": (90, 50, 120)}
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
WHITE_SQUARE = (240, 217, 181)
BLACK_SQUARE = (181, 136, 99)

ARROW_COLOUR = (0, 48, 136, 102)
BLOB_COLOUR = (20, 85, 30, 127)
CAPTURE_COLOUR = (20, 85, 0, 76)

FONT = "Noto_Sans/NotoSans-Regular.ttf"

FALLBACK_PIECE_FONT = pygame.font.Font(FONT, 800)


def piece_image(name, betza, colour):
    try:
        image = pygame.image.load(
            f"pieces/png/{name.replace(' ', '_')}_{'wb'[colour]}.png"
        )
    except FileNotFoundError:
        text = FALLBACK_PIECE_FONT.render(betza, True, (WHITE, BLACK)[colour])
        size = Vector(*text.get_size())
        image = pygame.Surface([max(size) for _ in range(2)], pygame.SRCALPHA)
        image.blit(text, ((Vector(*image.get_size()) - size) / 2).round())
    return image.convert_alpha()


class PieceImage:
    def __init__(self, piece):
        self.piece = piece
        self.white_image = piece_image(self.piece.name, self.piece.betza, W)
        self.black_image = piece_image(self.piece.name, self.piece.betza, B)
        self.current_size = None

    def generate_images(self, square_size):
        self.current_size = square_size
        size = (self.current_size, self.current_size)
        self.scaled_w = pygame.transform.smoothscale(self.white_image, size)
        self.scaled_b = pygame.transform.smoothscale(self.black_image, size)

    def image(self, colour, square_size):
        if square_size != self.current_size:
            self.generate_images(square_size)
        return (self.scaled_w, self.scaled_b)[colour]


class GUIBoard(chess.Board):
    def __init__(self, *args, pieces, **kwargs):
        self.pieces = pieces
        super().__init__(*args, **kwargs)

    @property
    def rects(self):
        return {
            self.square(file_, rank): (
                self.square_size * file_,
                self.square_size * (self.height - rank - 1),
                self.square_size,
                self.square_size,
            )
            for (file_, rank) in itertools.product(
                range(self.width), range(self.height)
            )
        }

    def draw(self, surface, highlight=None):
        pygame.draw.rect(
            surface,
            BLACK_SQUARE,
            (0, 0, self.width * self.square_size, self.height * self.square_size),
        )
        for square, rect in self.rects.items():
            try:
                square_surface = surface.subsurface(rect)
            except ValueError:
                continue
            square_surface.fill(WHITE_SQUARE if square.is_white else BLACK_SQUARE)
            if highlight is not None and square in highlight:
                if self[square]:
                    draw_capture_marks(square_surface)
                else:
                    draw_blob(square_surface)
            if self[square]:
                self.draw_piece_to_square(square_surface, self[square])

    def draw_piece_to_square(self, surface, piece):
        piece_image = piece_renderer.draw(piece, self.square_size)
        surface.blit(
            piece_image,
            (
                (Vector(*surface.get_size()) - Vector(*piece_image.get_size())) / 2
            ).round(),
        )

    def piece_clicked(self, mouse_pos, piece=None):
        square = self.square_from_mouse_pos(mouse_pos)
        if square is OFF_BOARD:
            return OFF_BOARD
        old_piece = self[square]
        if piece is not None:
            self[square] = piece
        if piece is CLEAR_SQUARE:
            self[square] = None
        return old_piece

    def square_from_mouse_pos(self, mouse_pos):
        for square, rect in self.rects.items():
            if all(rect[i] < mouse_pos[i] < rect[i] + rect[i + 2] for i in (0, 1)):
                return square
        return OFF_BOARD


class PieceShop(GUIBoard):
    def __init__(self, *args, pieces, **kwargs):
        super().__init__(*args, pieces=pieces, **kwargs)
        for i, piece in enumerate(pieces):
            y, x = divmod(i, self.width)
            if y >= self.height:
                return
            self[self.square(x, y)] = piece.create(W)

    def swap_colours(self):
        for square in self:
            if self[square] is not None:
                self[square].colour = 1 - self[square].colour


@dataclass
class PieceRenderer:
    images: dict[str, PieceImage] = field(default_factory=dict)

    def draw(self, piece, size):
        if piece.piece_type not in self.images:
            self.images[piece.piece_type] = PieceImage(piece.piece_type)

        return self.images[piece.piece_type].image(piece.colour, size)


piece_renderer = PieceRenderer()


def set_icon(piece):
    size = 256
    icon = pygame.Surface((size, size))
    icon.fill(WHITE_SQUARE)
    icon.blit(piece_renderer.draw(piece.create(B), size), (0, 0))
    pygame.display.set_icon(icon)


def format_time(centiseconds):
    seconds = int(centiseconds) / 100
    if seconds < 10:
        return f"{seconds:.1f} s"
    if seconds < 6000:
        return f"{int(seconds)} s"
    if seconds < 360000:
        return f"{seconds // 3600}:{int((seconds / 60) % 60)}"
    return f"{seconds // 3600} h"


def get_prefix(number):
    prefix = 0
    while number >= 1000:
        number /= 1000
        prefix += 1
    return number, ("", "k", "M", "G", "T", "P", "E", "Z", "Y")[prefix]


def format_nodes(nodes):
    nodes, prefix = get_prefix(int(nodes))
    return f"{nodes:#.3g} {prefix}N"


def format_nps(nps):
    nps, prefix = get_prefix(int(nps))
    return f"{nps:#.3g} {prefix}N ∕ s"


def get_analysis_text(analysis, move):
    if analysis is None:
        return ["Loading…"]
    try:
        score = int(analysis[1])
    except ValueError:
        return ["Loading…"]
    if move == B:
        score = -score
    if score > 100000:
        score = f"#+{score - 100000}"
    elif score < -100000:
        score = f"#{score + 100000}"
    else:
        score = f"{score / 100:.2f}"
    if "-" in score:
        score = score.replace("-", "\N{minus sign}")
    elif score == "0.00":
        score = "±0.00"
    elif "+" not in score:
        score = "+" + score
    return [
        str(score),
        f"at depth {analysis[0]}",
        f"in {format_time(analysis[2])}.",
        "Analysed",
        format_nodes(analysis[3]) + " at",
        format_nps(analysis[5]) + ".",
    ]


def render_lines(surface, lines, font, line_height):
    for i, line in enumerate(lines):
        surface.blit(font.render(line, True, BLACK), (0, i * line_height))


def draw_blob(surface):
    size = surface.get_size()[0]
    new_ss = size * AA
    new_surf = pygame.Surface(
        (Vector(*surface.get_size()) * AA).round(), pygame.SRCALPHA
    )
    width = 0.28 * new_ss
    pygame.draw.circle(
        new_surf, BLOB_COLOUR, (new_ss // 2, new_ss // 2), round(width / 2)
    )
    surface.blit(pygame.transform.smoothscale(new_surf, surface.get_size()), (0, 0))


def draw_capture_marks(surface):
    size = surface.get_size()[0]
    new_ss = size * AA
    new_surf = pygame.Surface(
        (Vector(*surface.get_size()) * AA).round(), pygame.SRCALPHA
    )
    new_surf.fill(CAPTURE_COLOUR)
    width = 1.13 * new_ss
    pygame.draw.circle(
        new_surf, (0, 0, 0, 0), (new_ss // 2, new_ss // 2), round(width / 2)
    )
    surface.blit(pygame.transform.smoothscale(new_surf, surface.get_size()), (0, 0))


def draw_arrow(surface, tail: Vector, head: Vector, size):
    head *= AA
    tail *= AA
    new_ss = size * AA
    new_surf = pygame.Surface(
        (Vector(*surface.get_size()) * AA).round(), pygame.SRCALPHA
    )
    direction = (head - tail).hat()
    width = new_ss / 4
    pygame.draw.circle(new_surf, ARROW_COLOUR, tail.round(), round(width / 2))
    pygame.draw.polygon(
        new_surf,
        ARROW_COLOUR,
        [
            p.round()
            for p in (
                head,
                head - new_ss * (direction * 0.6 - direction.perp() / 2),
                head - new_ss * direction * 0.6 + direction.perp() * width / 2,
                tail + direction.perp() * width / 2,
                tail - direction.perp() * width / 2,
                head - new_ss * direction * 0.6 - direction.perp() * width / 2,
                head - new_ss * (direction * 0.6 + direction.perp() / 2),
            )
        ],
    )
    surface.blit(
        pygame.transform.smoothscale(new_surf, surface.get_size())
        if AA != 1
        else new_surf,
        (0, 0),
    )


def render_move_arrow(surface, from_square, to_square, size):
    tail, head = (
        Vector(square.file_num + 0.5, square.board_height - (square.rank_num + 0.5))
        * size
        for square in (from_square, to_square)
    )
    draw_arrow(surface, tail, head, size)


def blit_alpha(surface, image, opacity, position=(0, 0)):
    surface_copy = surface.copy()
    surface_copy.blit(image, position)
    surface_copy.set_alpha(opacity)
    surface.blit(surface_copy, (0, 0))


class Highlight(chess.Board):
    def __init__(self, fen):
        fen = fen.split(" ")
        assert fen[0] == "highlight"
        fen = fen[1].strip()
        height = fen.count("/") + 1
        width = 0
        for char in fen.split("/")[0]:
            if char in "123456789":
                width += int(char)
            else:
                width += 1
        super().__init__(width, height)
        file_ = 0
        rank = height - 1
        self.any = False
        for char in fen:
            if char == "/":
                file_ = 0
                rank -= 1
            elif char in "123456789":
                file_ += int(char)
            else:
                self[self.square(file_, rank)] = char
                file_ += 1
                self.any = True

    def __contains__(self, value):
        return self[value] is not None

    def __bool__(self):
        return self.any


def require_enabled(function):
    def new_function(self, *args, **kwargs):
        if not self.enabled:
            return False
        return function(self, *args, **kwargs)

    return new_function


class Button:
    def __init__(self, text, position):
        self.state = "default"
        self.text = text
        self.position = position
        self.update_image()
        self.enabled = True

    def draw(self, surface):
        """Render the button to a surface."""
        surface.blit(self.image, self.position)

    def is_mouse_in(self, mouse_pos):
        return (
            self.position[0] < mouse_pos[0] < self.position[0] + self.image.get_width()
            and self.position[1]
            < mouse_pos[1]
            < self.position[1] + self.image.get_height()
        )

    @require_enabled
    def mouse_over(self, mouse_pos):
        """Change state if the button is hovered over."""
        if self.state != "active":
            previous = self.state
            self.state = "hover" if self.is_mouse_in(mouse_pos) else "default"

            if self.state != previous:
                self.update_image()
        return self.state == "hover"

    @require_enabled
    def click(self, mouse_pos):
        """Activate upon being clicked."""
        if self.is_mouse_in(mouse_pos):
            self.state = "active"
            self.update_image()

    @require_enabled
    def release(self, mouse_pos):
        """Return True if the button has been clicked."""
        if self.state == "active":
            self.state = "default"
            self.update_image()
            if self.is_mouse_in(mouse_pos):
                return True
        return False


class TextButton(Button):
    def __init__(self, *args, font, colours, **kwargs):
        self.font = font
        self.colours = colours
        super().__init__(*args, **kwargs)

    def update_image(self):
        self.image = self.font.render(self.text, True, self.colours[self.state])


class Switch(Button):
    def __init__(self, *args, options, **kwargs):
        self.options = list(options)
        self._index = 0
        super().__init__(*args, text=self.text, **kwargs)

    @property
    def index(self):
        return self._index

    @index.setter
    def index(self, value):
        if not 0 <= value < len(self.options):
            raise ValueError("Invalid index")
        self._index = value
        self.update_image()

    @property
    def text(self):
        return self.options[self.index]

    @text.setter
    def text(self, value):
        self.options[self.index] = value

    def release(self, mouse_pos):
        result = super().release(mouse_pos)
        if result:
            self.index = (self.index + 1) % len(self.options)
            self.update_image()
        return result


class TextSwitch(Switch, TextButton):
    pass
