from __future__ import annotations

import math
import string
import time
from dataclasses import dataclass, field

FILES = "abcdefghijklmnopqrstuvwxyzαβγδεζηθικλμνξοπρστυφχψω"

W = 0
B = 1


def to_camel(string):
    first, *rest = string.split()
    return "".join((first.lower(), *(w.capitalize() for w in rest)))


SAME_AS_NAME = object()


@dataclass(frozen=True)
class PieceType:
    name: str
    betza: str | None
    stockfish_name: str | None = field(default=SAME_AS_NAME)

    def __post_init__(self):
        if self.stockfish_name is SAME_AS_NAME:
            object.__setattr__(self, "stockfish_name", to_camel(self.name))

    def create(self, colour):
        return Piece(self, colour)

    @property
    def best_letters(self):
        result = self.name + string.ascii_uppercase
        if self.betza is not None and len(self.betza) == 1:
            result = self.betza + result
        return result.upper()


@dataclass
class Piece:
    piece_type: PieceType
    colour: int

    @property
    def name(self):
        return self.piece_type.name

    def copy(self):
        return self.__class__(self.piece_type, self.colour)


@dataclass(frozen=True)
class Square:
    file_num: int
    rank_num: int
    board_width: int
    board_height: int

    @classmethod
    def from_text(cls, text, board_width, board_height):
        return cls(
            FILES.index(text[0]),
            int(text[1:]) - (1 if board_height < 10 else 0),
            board_width,
            board_height,
        )

    @property
    def rank(self) -> int:
        return self.rank_num + (1 if self.board_height < 10 else 0)

    @property
    def file(self) -> str:
        return FILES[self.file_num]

    @property
    def is_white(self) -> bool:
        return bool((self.file_num + self.rank_num) % 2)

    @property
    def index(self):
        return self.file_num * self.board_height + self.rank_num

    def __add__(self, value: tuple):
        if not (
            0 <= self.file_num + value[0] < self.board_width
            and 0 <= self.rank_num + value[1] < self.board_height
        ):
            raise ValueError("Square moved off board")
        return Square(
            self.file_num + value[0],
            self.rank_num + value[1],
            self.board_width,
            self.board_height,
        )

    def __sub__(self, value):
        if isinstance(value, Square):
            return (self.file_num - value.file_num, self.rank_num - value.rank_num)
        return self.__add__((-value[0], -value[1]))

    def __str__(self):
        return f"{self.file}{self.rank}"


@dataclass
class Board:
    width: int
    height: int
    squares: list[Square | None] = field(init=False)

    def __post_init__(self):
        self.squares = [None for _ in range(self.width * self.height)]

    def __getitem__(self, square):
        return self.squares[square.index]

    def __setitem__(self, square, item):
        self.squares[square.index] = item

    def __iter__(self):
        return (
            self.square(file_, rank)
            for file_ in range(self.width)
            for rank in range(self.height)
        )

    def __len__(self):
        return self.width * self.height

    def square(self, file_, rank):
        return Square(file_, rank, self.width, self.height)


@dataclass
class Clock:
    time: float
    increment: float
    start_time: float | None = None

    def start(self):
        self.start_time = time.time()

    def end(self):
        self.time += self.increment - self.delta
        self.start_time = None

    @property
    def delta(self):
        return max(time.time() - self.start_time - 0.03, 0)

    @property
    def current_time(self):
        if self.start_time is None:
            return self.time
        return self.time - self.delta

    def __str__(self):
        mins, secs = divmod(math.ceil(self.current_time), 60)
        value = f"{mins:02}:{secs:02}"
        if self.increment:
            value += f" (+{self.increment})"
        return value
