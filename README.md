# Fairyboard

Fairyboard is a GUI frontend for [Fairy
Stockfish](https://github.com/ianfab/Fairy-Stockfish).

The dependencies are:

- Python 3.8 or later
- Fairy-Stockfish (compile with `largeboards=yes` if you want >8×8
  boards. `all=yes` just causes a slowdown, because Fairyboard does not
  support Game of the Amazons.)
- The Python dependencies in `requirements.txt`

Launch Fairyboard with `./app`.

The piece graphics are based on
[Cburnett's](https://en.wikipedia.org/wiki/User:Cburnett/GFDL_images/Chess),
licensed under the GPLv2+ (amongst other licences).

---

Fairyboard
Copyright (C) 2020–2022 Tom Fryers

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
