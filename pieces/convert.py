#!/usr/bin/env python3
import glob
import subprocess
import time

files = glob.glob("svg/*.svg")
SIZE = 1024
processes = []
for f in files:
    processes.append(
        subprocess.Popen(
            [
                "inkscape",
                "-w",
                str(SIZE),
                "-h",
                str(SIZE),
                f,
                "--export-filename",
                f.replace("svg", "png"),
                "--export-png-color-mode=GrayAlpha_8",
            ]
        )
    )
while processes:
    processes = [p for p in processes if p.poll() is None]
    time.sleep(0.2)
